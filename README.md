# SmartPesa Android Codestyle #

Android Studio/ IntelliJ IDEA codestyle settings for SmartPesa

## Installation ##
### For Mac OS ###
1. Copy *codestyles* folder and its content into *~/Library/Preferences/AndroidStudio(versionNumber)/*
    * Close *Android Studio* if it's running.
2. Open *Android Studio*, Go to *Preferences/Editor/Code Style*
3. On *Scheme: *, Select *SmartPesaStyle*